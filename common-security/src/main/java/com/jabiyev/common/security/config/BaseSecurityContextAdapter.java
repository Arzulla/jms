package com.jabiyev.common.security.config;

import com.jabiyev.common.security.UserAuthorities;
import com.jabiyev.common.security.config.auth.AuthFilterConfigurerAdapter;
import com.jabiyev.common.security.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;
import java.util.StringJoiner;

@Slf4j
@RequiredArgsConstructor
public abstract class BaseSecurityContextAdapter extends WebSecurityConfigurerAdapter {

    private final List<AuthService> authServices;
    private final SecurityProperties securityProperties;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors().configurationSource(corsConfigurationSource());
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));

        http.apply(new AuthFilterConfigurerAdapter(authServices));
        super.configure(http);
    }

    protected CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration corsConfiguration = securityProperties.getCors();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);
        return source;
    }

    protected String authority(String role) {
        return "hasAuthority('" + role + "')";
    }

    protected String authority(UserAuthorities role) {
        return "hasAuthority('" + role.name() + "')";
    }

    protected String authorities(Object... roles) {
        StringJoiner joiner = new StringJoiner(" or ");
        for (Object role : roles) {
            if (role instanceof UserAuthorities) {
                joiner.add(authority((UserAuthorities) role));
            } else {
                joiner.add(authority(role.toString()));
            }
        }
        return joiner.toString();
    }
}
