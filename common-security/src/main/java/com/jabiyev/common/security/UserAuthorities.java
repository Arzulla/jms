package com.jabiyev.common.security;

public enum UserAuthorities {

    USER("ROLE_USER"),
    PUBLISHER("ROLE_PUBLISHER");

    private String role;

    UserAuthorities(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
