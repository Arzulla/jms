package service;

import com.jabiyev.book.managment.BookManagementMsApplication;
import com.jabiyev.book.managment.domain.Book;
import com.jabiyev.book.managment.domain.BookDetails;
import com.jabiyev.book.managment.domain.enums.BookType;
import com.jabiyev.book.managment.repository.BookRepository;
import com.jabiyev.book.managment.service.dto.book.BookDto;
import com.jabiyev.book.managment.service.dto.book.BookRequestDto;
import com.jabiyev.book.managment.service.dto.book.UpdateBookRequestDto;
import com.jabiyev.book.managment.service.dto.bookdetails.BookDetailsRequestDto;
import com.jabiyev.book.managment.service.exceptions.BookNotFoundException;
import com.jabiyev.book.managment.service.impl.BookServiceImpl;
import com.jabiyev.common.exceptions.PasswordDoNotMatchException;
import com.jabiyev.common.search.MatchType;
import com.jabiyev.common.search.SearchCriteria;
import com.jabiyev.common.search.SearchSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = BookManagementMsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BookServiceTest {

    private static final Long ID = 1L;
    private static final String username = "mockUser";

    private BookServiceImpl bookService;
    private ModelMapper modelMapper;

    private Book book;

    @Mock
    private BookRepository bookRepository;

    @BeforeEach
    void init() {
        modelMapper = new ModelMapper();
        bookService = new BookServiceImpl(bookRepository, modelMapper);
        book = createBook();
    }

    @Test
    void givenIdDoNotExistThenException() {
        //Assert
        assertThatThrownBy(() -> bookService.findById(ID))
                .isInstanceOf(BookNotFoundException.class);
    }

    @Test
    void givenValidIdThenResult() {
        //Arrange
        when(bookRepository.findById(ID)).thenReturn(Optional.of(book));

        //Act
        BookDto result = bookService.findById(ID);

        //Assert
        verify(bookRepository, times(1)).findById(ID);

        assertThat(result.getId()).isEqualTo(ID);
    }

    @Test
    @WithMockUser(username = "mockUser")
    void givenValidInputWhenCreateThenSave() {

        //Arrange
        BookRequestDto bookRequestDto = createBookRequestDto();
        Book book = modelMapper.map(bookRequestDto, Book.class);
        book.setId(ID);
        book.setPublisherId("mockUser");
        book.getBookDetails().setId(ID);

        when(bookRepository.save(any(Book.class))).thenReturn(book);

        //Act
        BookDto result = bookService.create(bookRequestDto);

        //Assert
        assertThat(result.getPublisherId()).isEqualTo("mockUser");
        assertThat(result.getId()).isEqualTo(ID);
        verify(bookRepository, times(1)).save(any(Book.class));
    }

    @Test
    void givenIdNotExistInUpdateThenException() {
        //Arrange
        UpdateBookRequestDto updateBookRequestDto = createUpdateBookRequestDto();

        //Assert
        assertThatThrownBy(() -> bookService.update(updateBookRequestDto))
                .isInstanceOf(BookNotFoundException.class);
    }

    @Test
    @WithMockUser(username = "anotherUser")
    void givenIdNotBelongToUserThenException() {
        //Arrange
        UpdateBookRequestDto updateBookRequestDto = createUpdateBookRequestDto();

        when(bookRepository.findById(ID)).thenReturn(Optional.of(book));

        //Act,Assert
        assertThatThrownBy(() -> bookService.update(updateBookRequestDto))
                .isInstanceOf(BookNotFoundException.class);

    }

    @Test
    @WithMockUser(username = username)
    void givenValidInputInUpdateThenSave() {
        //Arrange
        UpdateBookRequestDto updateBookRequestDto = createUpdateBookRequestDto();
        when(bookRepository.findById(updateBookRequestDto.getId())).thenReturn(Optional.of(book));

        //Act
        BookDto update = bookService.update(updateBookRequestDto);

        //Assert
        verify(bookRepository, times(1)).findById(updateBookRequestDto.getId());

        assertThat(update.getId()).isEqualTo(updateBookRequestDto.getId());
        assertThat(update.getBookDetails().getChapterSize()).isEqualTo(updateBookRequestDto.getRequestDto().getBookDetails().getChapterSize());
    }

    @Test
    void givenIdDoNotMatchThenException() {
        assertThatThrownBy(() -> bookService.delete(ID))
                .isInstanceOf(BookNotFoundException.class);
    }

    @Test
    @WithMockUser(username = "anotherUser")
    void givenIdNotBelongToUserInDeleteThenException() {
        //Arrange
        when(bookRepository.findById(ID)).thenReturn(Optional.of(book));

        //Act,Assert
        assertThatThrownBy(() -> bookService.delete(ID))
                .isInstanceOf(BookNotFoundException.class);

    }

    @Test
    @WithMockUser(username = username)
    void givenValidInputInDeleteThenDelete() {
        //Arrange
        when(bookRepository.findById(ID)).thenReturn(Optional.of(book));

        //Act
        bookService.delete(ID);

        //Assert
        verify(bookRepository, times(1)).deleteById(ID);

    }

    @Test
    void givenPublisherIdThenResult() {// doesn't work properly
        //Arrange
        Book book = createBook();
        Page<Book> mockPage = new PageImpl<>(List.of(book));
        var pageable = Pageable.ofSize(1);

        Mockito.lenient().when(bookRepository.findAllByPublisherId(username, pageable)).thenReturn(mockPage);

        //Act
        Page<BookDto> byPublisherId = bookService.findByPublisherId(username, pageable);

        //Assert
        assertThat(byPublisherId.getContent().get(0).getPublisherId()).isEqualTo(username);
    }

    @Test
    void givenGenericSearchDtoWhenSearchThenResult() {
        //Arrange
        when(bookRepository
                .findAll(any(SearchSpecification.class), any(Pageable.class)))
                .thenReturn(Page.empty());

        //Act
        bookService.search("", Pageable.unpaged());

        //Verify
        verify(bookRepository, times(1)).findAll(any(SearchSpecification.class), any(Pageable.class));
    }


    private Book createBook() {
        return Book.builder().id(ID).Author("Robert Kiyosaki and Sharon Lechter").name("Rich Dad,Poor Dad").publisherId(username)
                .bookDetails(BookDetails.builder().id(ID).chapterSize(20).pageSize(440).build()).build();
    }

    private BookRequestDto createBookRequestDto() {
        return BookRequestDto.builder().name("TestBook").author("TestAuthot")
                .bookDetails(BookDetailsRequestDto.builder().pageSize(450).chapterSize(22).build()).build();
    }

    private UpdateBookRequestDto createUpdateBookRequestDto() {
        return UpdateBookRequestDto.builder().id(ID).requestDto(createBookRequestDto()).build();
    }


}
