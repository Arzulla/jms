package rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jabiyev.book.managment.BookManagementMsApplication;
import com.jabiyev.book.managment.controller.BookController;
import com.jabiyev.book.managment.domain.Book;
import com.jabiyev.book.managment.domain.BookDetails;
import com.jabiyev.book.managment.service.dto.book.BookDto;
import com.jabiyev.book.managment.service.dto.bookdetails.BookDetailsDto;
import com.jabiyev.book.managment.service.impl.BookServiceImpl;
import com.jabiyev.common.security.UserAuthorities;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.*;


@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BookManagementMsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BookControllerTest {

    private static final Long ID = 1L;
    private static final String username = "mockUser";
    private static final String USER_AUTHORITY = "ROLE_USER";
    private static final String URL = "/api/book";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BookServiceImpl bookService;

    @Test
    @WithMockUser(username = username, authorities = {USER_AUTHORITY})
    public void givenIdWhenGetByIdThenOk() throws Exception {
        //Arrange
        BookDto responseDto = createBookDto();
        when(bookService.findById(ID))
                .thenReturn(responseDto);

        //Act
        ResultActions actions = mockMvc.perform(
                get(URL + "/" + ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(responseDto))
        );
        //Assert
        verify(bookService, Mockito.times(1))
                .findById(ID);
        actions.andExpect(status().isOk());

    }

    private BookDto createBookDto() {
        return BookDto.builder().id(ID).Author("Robert Kiyosaki and Sharon Lechter").name("Rich Dad,Poor Dad").publisherId(username)
                .bookDetails(BookDetailsDto.builder().id(ID).chapterSize(20).pageSize(440).build()).build();
    }


}
