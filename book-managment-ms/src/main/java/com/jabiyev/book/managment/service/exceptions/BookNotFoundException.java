package com.jabiyev.book.managment.service.exceptions;

import com.jabiyev.common.exceptions.NotFoundException;

public class BookNotFoundException extends NotFoundException {

    public static final String MESSAGE = "Book with id %s not found";
    private static final long serialVersionUID = 5843213248811L;

    public BookNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }

}
