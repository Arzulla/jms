package com.jabiyev.book.managment.service.dto.book;

import com.jabiyev.book.managment.domain.enums.BookType;
import com.jabiyev.book.managment.service.dto.bookdetails.BookDetailsDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDto {

    private Long id;

    private String name;

    private String Author;

    private BookType type;

    private BookDetailsDto bookDetails;

    private String publisherId;
}
