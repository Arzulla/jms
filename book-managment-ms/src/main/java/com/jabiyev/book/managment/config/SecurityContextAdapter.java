package com.jabiyev.book.managment.config;

import com.jabiyev.common.security.UserAuthorities;
import com.jabiyev.common.security.config.BaseSecurityContextAdapter;
import com.jabiyev.common.security.config.SecurityProperties;
import com.jabiyev.common.security.service.AuthService;
import com.jabiyev.common.security.service.jwt.JwtService;
import com.jabiyev.common.security.service.jwt.JwtTokenAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@Slf4j
@EnableWebSecurity
@Import({SecurityProperties.class, JwtService.class, JwtTokenAuthService.class})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityContextAdapter extends BaseSecurityContextAdapter {

    public SecurityContextAdapter(List<AuthService> authServices, SecurityProperties securityProperties) {
        super(authServices, securityProperties);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    private final String BOOK_API = "/api/book";


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, BOOK_API).access(authorities(UserAuthorities.PUBLISHER.getRole()))
                .antMatchers(HttpMethod.PUT, BOOK_API).access(authorities(UserAuthorities.PUBLISHER.getRole()))
                .antMatchers(HttpMethod.DELETE, BOOK_API + "/**").access(authorities(UserAuthorities.PUBLISHER.getRole()))

                .antMatchers(HttpMethod.GET, BOOK_API + "**").authenticated();


        super.configure(http);
    }
}
