package com.jabiyev.book.managment.service;

import com.jabiyev.book.managment.service.dto.book.BookDto;
import com.jabiyev.book.managment.service.dto.book.BookRequestDto;
import com.jabiyev.book.managment.service.dto.book.PublishRequestDto;
import com.jabiyev.book.managment.service.dto.book.UpdateBookRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {
    List<BookDto> findAll();

    BookDto findById(Long id);

    Page<BookDto> findByPublisherId(String publisherId,Pageable Pageable);

    Page<BookDto> search(String query, Pageable pageable);

    BookDto create(BookRequestDto book);

    BookDto update(UpdateBookRequestDto updateBookRequestDto);

    void delete(Long id);

}
