package com.jabiyev.book.managment.service.dto.bookdetails;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDetailsDto {
    private Long id;
    private Integer pageSize;
    private Integer chapterSize;
}
