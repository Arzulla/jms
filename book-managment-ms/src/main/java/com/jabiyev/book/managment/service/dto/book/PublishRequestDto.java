package com.jabiyev.book.managment.service.dto.book;

import lombok.Data;

@Data
public class PublishRequestDto {
    private  Long bookId;
    private  Long publisherId;
}
