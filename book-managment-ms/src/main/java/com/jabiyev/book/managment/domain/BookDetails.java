package com.jabiyev.book.managment.domain;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@ToString(exclude = "book")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = BookDetails.TABLE_NAME)
public class BookDetails {

    public static final String TABLE_NAME = "book_details";
    @Id
    @Column(name = "book_id")
    private Long id;

    @Column(name = "pageSize")
    private Integer pageSize;

    @Column(name = "chapterSize")
    private Integer chapterSize;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "book_id")
    private Book book;

}
