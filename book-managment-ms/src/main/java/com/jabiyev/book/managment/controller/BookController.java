package com.jabiyev.book.managment.controller;

import com.jabiyev.book.managment.service.BookService;
import com.jabiyev.book.managment.service.dto.book.BookDto;
import com.jabiyev.book.managment.service.dto.book.BookRequestDto;
import com.jabiyev.book.managment.service.dto.book.PublishRequestDto;
import com.jabiyev.book.managment.service.dto.book.UpdateBookRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping()
    public ResponseEntity<List<BookDto>> get() {
        log.info("Get All");
        var result = bookService.findAll();
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDto> get(@PathVariable Long id) {
        log.info("Get by id: {} ", id);
        var result = bookService.findById(id);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/publish/{id}")
    public ResponseEntity<Page<BookDto>> getByPublisherId(@PathVariable String id,
                                                          Pageable pageable) {
        log.info("Get by id: {} ", id);
        var result = bookService.findByPublisherId(id, pageable);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/search")
    public ResponseEntity<Page<BookDto>> search(@RequestParam(required = false, defaultValue = "") String query,
                                                Pageable pageable) {

        log.info("Search  by id: {} ", query);
        Page<BookDto> result = bookService.search(query, pageable);
        return ResponseEntity.ok(result);
    }

    @PostMapping()
    public ResponseEntity<BookDto> post(@RequestBody BookRequestDto requestDto) {
        log.info("Creating book : {}", requestDto);
        BookDto result = bookService.create(requestDto);
        return ResponseEntity.ok(result);
    }

    @PutMapping()
    public ResponseEntity<BookDto> put(@RequestBody UpdateBookRequestDto requestDto, Authentication authentication) {
        log.info("Update book: {}", requestDto);

        BookDto result = bookService.update(requestDto);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public  ResponseEntity delete(@PathVariable Long id){
        log.info("Delete by {} ",id);
        bookService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
