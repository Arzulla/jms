package com.jabiyev.book.managment.service.dto.book;

import com.jabiyev.book.managment.domain.enums.BookType;
import com.jabiyev.book.managment.service.dto.bookdetails.BookDetailsRequestDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookRequestDto {

    private String name;

    private String author;

    private BookType type;

    private BookDetailsRequestDto bookDetails;
}
