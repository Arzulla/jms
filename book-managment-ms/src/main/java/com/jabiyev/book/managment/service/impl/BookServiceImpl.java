package com.jabiyev.book.managment.service.impl;

import com.jabiyev.book.managment.domain.Book;
import com.jabiyev.book.managment.domain.BookDetails;
import com.jabiyev.book.managment.repository.BookRepository;
import com.jabiyev.book.managment.service.exceptions.BookNotFoundException;
import com.jabiyev.common.search.MatchType;
import com.jabiyev.common.search.SearchCriteria;
import com.jabiyev.common.search.SearchOperation;
import com.jabiyev.common.search.SearchSpecification;
import com.jabiyev.book.managment.service.BookService;
import com.jabiyev.book.managment.service.dto.book.BookDto;
import com.jabiyev.book.managment.service.dto.book.BookRequestDto;
import com.jabiyev.book.managment.service.dto.book.PublishRequestDto;
import com.jabiyev.book.managment.service.dto.book.UpdateBookRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public List<BookDto> findAll() {
        var result = bookRepository.findAll().stream()
                .map(book -> modelMapper.map(book, BookDto.class))
                .collect(Collectors.toList());

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public BookDto findById(Long id) {
        BookDto result = bookRepository.findById(id).map(book -> modelMapper.map(book, BookDto.class)).orElseThrow(
                () -> new BookNotFoundException(id));
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BookDto> findByPublisherId(String publisherId, Pageable pageable) {
        var result = bookRepository.findAllByPublisherId(publisherId, pageable)
                .map(book -> modelMapper.map(book, BookDto.class));

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BookDto> search(String query, Pageable pageable) {

        var searchSpec = List.of(searchCriteria("name", query));

        var result = bookRepository
                .findAll(new SearchSpecification<>(searchSpec, MatchType.OR), pageable)
                .map(book -> modelMapper.map(book, BookDto.class));
        return result;
    }

    @Override
    @Transactional
    public BookDto create(BookRequestDto createDto) {
        log.info("===   Book Service.Create =====");

        Book book = modelMapper.map(createDto, Book.class);
        book.setBookDetailsBiDirectional(modelMapper.map(createDto.getBookDetails(), BookDetails.class));

        log.info("Mapper: {}", book);

        String publisherName = SecurityContextHolder.getContext().getAuthentication().getName();

        log.info("Publisher: {}", publisherName);
        book.setPublisherId(publisherName);

        Book result = bookRepository.save(book);
        log.info("result: {}", book);

        return modelMapper.map(result, BookDto.class);
    }

    @Override
    @Transactional
    public BookDto update(UpdateBookRequestDto request) {
        log.info("===   Book Service.Update =====");

        Book updatedBook = bookRepository.findById(request.getId()).orElseThrow(
                () -> new BookNotFoundException(request.getId()));

        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        if (!updatedBook.getPublisherId().equalsIgnoreCase(username)) {
            throw new BookNotFoundException(request.getId());
        }

        var book = request.getRequestDto();

        updatedBook.setName(book.getName());
        updatedBook.setType(book.getType());
        updatedBook.setAuthor(book.getAuthor());

        BookDetails bookDetails = updatedBook.getBookDetails();

        bookDetails.setPageSize(book.getBookDetails().getPageSize());
        bookDetails.setChapterSize(book.getBookDetails().getChapterSize());
        return modelMapper.map(updatedBook, BookDto.class);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(id));

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!book.getPublisherId().equalsIgnoreCase(username)) {
            throw new BookNotFoundException(id);
        }
        bookRepository.deleteById(id);
    }


    private SearchCriteria searchCriteria(String key, String value) {
        return SearchCriteria
                .builder()
                .key(key)
                .value(value)
                .operation(SearchOperation.MATCH)
                .build();
    }

}
