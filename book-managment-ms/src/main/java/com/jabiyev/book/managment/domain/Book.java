package com.jabiyev.book.managment.domain;

import com.jabiyev.book.managment.domain.enums.BookType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = Book.TABLE_NAME)
public class Book {

    public static final String TABLE_NAME = "Books";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "Author")
    private String Author;

    @Column(name = "type")
    private BookType type;

    @OneToOne(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private BookDetails bookDetails;

    @Column(name = "publisher_Id")
    private String publisherId;

    public void setBookDetailsBiDirectional(BookDetails bookDetails){
        setBookDetails(bookDetails);
        bookDetails.setBook(this);
    }

}
