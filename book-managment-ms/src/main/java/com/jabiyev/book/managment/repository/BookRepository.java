package com.jabiyev.book.managment.repository;

import com.jabiyev.book.managment.domain.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {

   @EntityGraph(attributePaths = {"bookDetails"})
   List<Book> findAll();

   @EntityGraph(attributePaths = {"bookDetails"})
   Optional<Book> findById(Long id);

   Page<Book> findAllByPublisherId(String id,Pageable pageable);
}
