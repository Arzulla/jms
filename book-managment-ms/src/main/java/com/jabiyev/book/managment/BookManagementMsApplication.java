package com.jabiyev.book.managment;

import com.jabiyev.book.managment.service.BookService;
import com.jabiyev.book.managment.service.dto.book.PublishRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class BookManagementMsApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(BookManagementMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {


    }
}
