package com.jabiyev.book.managment.service.dto.bookdetails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookDetailsRequestDto {
    private Integer pageSize;
    private Integer chapterSize;
}
