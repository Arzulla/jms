package rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.jabiyev.common.exceptions.ValidationException;
import com.jabiyev.user.managment.UserManagementMsApplication;
import com.jabiyev.user.managment.service.dto.request.SignUpDto;
import com.jabiyev.user.managment.service.impl.UserServiceImpl;
import com.jabiyev.user.managment.web.rest.AuthController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@WebMvcTest
class AuthControllerTest {

    @MockBean
    private UserServiceImpl authService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

   /* @Test
    void whenValidDtoThenBadRequest() throws Exception {
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setUsername("cebiyeverzulla044@gmail.com");
        signUpDto.setPassword("Hakuna1234");
        signUpDto.setPasswordConf("Hakuna1234");

        SignUpDto savedSignUpDto = new SignUpDto();
        savedSignUpDto.setUsername("cebiyeverzulla044@gmail.com");
        savedSignUpDto.setPassword("Hakuna1234");
        savedSignUpDto.setPasswordConf("Hakuna1234");
        savedSignUpDto.setId(1L);

        when(authService.signUp(signUpDto)).thenReturn(savedSignUpDto);

        //Act
        mockMvc.perform(
                        post("/v1/auth/sign-up")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(signUpDto))
                ).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(savedSignUpDto)));

    }
*/
    @Test
    void whenInvalidDtoThenBadRequest() throws Exception {
        //Arrange
        SignUpDto signUpDto = new SignUpDto();

        //Act
        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpDto))
        ).andExpect(status().isBadRequest());


    }

    @Test
    void whenPasswordDontMatchThenBadRequest() throws Exception {
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setPassword("12345678");
        signUpDto.setPasswordConf("234332");


        var result = new ValidationException("Password do not match");

        when(authService.signUp(signUpDto)).thenThrow(result);

        //Act
        mockMvc.perform(
                        post("/v1/auth/sign-up")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(signUpDto))
                ).andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(result.getMessage()));

    }

}
