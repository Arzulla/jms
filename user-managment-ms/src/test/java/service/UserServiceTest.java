package service;

import com.jabiyev.common.exceptions.PasswordDoNotMatchException;
import com.jabiyev.common.exceptions.ValidationException;
import com.jabiyev.user.managment.repository.AuthorityRepository;
import com.jabiyev.user.managment.repository.UserRepository;
import com.jabiyev.user.managment.service.UserService;
import com.jabiyev.user.managment.service.dto.request.SignUpDto;
import com.jabiyev.user.managment.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private UserServiceImpl userService;
    private ModelMapper modelMapper;

    @Mock
    private UserRepository userRepository;


    @Mock
    private AuthorityRepository authorityRepository;

    @BeforeEach
    void init() {
        modelMapper = new ModelMapper();
        userService = new UserServiceImpl(userRepository, modelMapper, null, authorityRepository);
    }

    @Test
    void givenPasswordDoNotMatchThenException() {
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setPassword("123456");
        signUpDto.setPasswordConf("123456A");

        assertThatThrownBy(() -> userService.signUp(signUpDto))
                .isInstanceOf(PasswordDoNotMatchException.class);

    }

    @Test
    void givenInvalidEmailThenException() {
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setUsername("admin");
        signUpDto.setPassword("@Ab123456");
        signUpDto.setPasswordConf("@Ab123456");

        //Act & Assert
        assertThatThrownBy(() -> userService.signUp(signUpDto))
                .isInstanceOf(ValidationException.class)
                .hasMessage("Argument validation failed");

    }


}
