package com.jabiyev.user.managment.config;

import com.jabiyev.common.security.UserAuthorities;
import com.jabiyev.common.security.config.BaseSecurityContextAdapter;
import com.jabiyev.common.security.config.SecurityProperties;
import com.jabiyev.common.security.service.AuthService;
import com.jabiyev.common.security.service.jwt.JwtService;
import com.jabiyev.common.security.service.jwt.JwtTokenAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@Slf4j
@EnableWebSecurity
@Import({SecurityProperties.class, JwtService.class, JwtTokenAuthService.class})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityContextAdapter extends BaseSecurityContextAdapter {

    public SecurityContextAdapter(List<AuthService> authServices, SecurityProperties securityProperties) {
        super(authServices, securityProperties);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // All user
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/auth/authenticate").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/auth/sign-up").permitAll();

        //Only authorize persons
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/auth/add-role").hasAuthority(UserAuthorities.PUBLISHER.getRole());


        super.configure(http);
    }


}
