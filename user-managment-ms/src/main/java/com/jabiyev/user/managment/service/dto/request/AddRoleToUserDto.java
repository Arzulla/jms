package com.jabiyev.user.managment.service.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddRoleToUserDto {

    @NotNull
    private String username;

    @NotNull
    private String authority;
}
