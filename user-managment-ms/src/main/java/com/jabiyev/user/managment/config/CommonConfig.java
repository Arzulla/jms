package com.jabiyev.user.managment.config;

import com.jabiyev.common.exceptions.GlobalExceptionHandler;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({GlobalExceptionHandler.class})
public class CommonConfig {

    @Bean
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }
}
