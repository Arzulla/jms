package com.jabiyev.user.managment.service.exceptions;

import com.jabiyev.common.exceptions.NotFoundException;

public class AuthorityNotFoundException extends NotFoundException {

    public static final String MESSAGE = "Authority with name %s not found";
    private static final long serialVersionUID = 5843213248811L;

    public AuthorityNotFoundException(String authority) {
        super(String.format(MESSAGE, authority));
    }

}