package com.jabiyev.user.managment.service.impl;

import com.jabiyev.common.exceptions.ErrorsExtension;
import com.jabiyev.common.exceptions.PasswordDoNotMatchException;
import com.jabiyev.common.exceptions.ValidationException;
import com.jabiyev.common.security.UserAuthorities;
import com.jabiyev.common.validations.Validation;
import com.jabiyev.user.managment.domain.User;
import com.jabiyev.user.managment.domain.UserAuthority;
import com.jabiyev.user.managment.service.dto.request.AddRoleToUserDto;
import com.jabiyev.user.managment.service.dto.request.SignUpDto;
import com.jabiyev.user.managment.service.dto.request.SignUpResponseDto;
import com.jabiyev.user.managment.repository.AuthorityRepository;
import com.jabiyev.user.managment.repository.UserRepository;
import com.jabiyev.user.managment.service.UserService;
import com.jabiyev.user.managment.service.exceptions.AuthorityNotFoundException;
import com.jabiyev.user.managment.service.exceptions.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AuthorityRepository authorityRepository;

    @Override
    @Transactional
    public User loadUserByUsername(String username) {
        log.info("Loading user by username {}", username);
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));

    }

    @Transactional
    public SignUpResponseDto signUp(SignUpDto signUpDto) {
        if (!signUpDto.getPassword().equals(signUpDto.getPasswordConf())) {
            throw new PasswordDoNotMatchException();
        }

        ErrorsExtension errorsExtension = new ErrorsExtension();
        List<ErrorsExtension> errors = new ArrayList<>();

        if (signUpDto.getPassword().length() >= 8 && signUpDto.getPassword().length() <= 30) {
            if (Validation.isNotNullOrEmpty(signUpDto.getUsername())) {
                if (Validation.isEmail(signUpDto.getUsername())) {

                    Optional<User> byUsername = userRepository.findByUsername(signUpDto.getUsername().toUpperCase());

                    if (byUsername.isPresent()) {
                        throw new ValidationException(String.format("Email already exists with %s ", signUpDto.getUsername()));
                    } else {

                        signUpDto.setUsername(signUpDto.getUsername().toUpperCase());
                        User user = signUpToUser(signUpDto);

                        UserAuthority userAuthority = authorityRepository.findByAuthority(UserAuthorities.USER.getRole())
                                .orElseThrow(() -> new AuthorityNotFoundException(UserAuthorities.USER.getRole()));

                        user.addRole(userAuthority);
                        User saved = userRepository.save(user);
                        return mapper.map(saved, SignUpResponseDto.class);
                    }

                } else {
                    errorsExtension.setProperty("email");
                    errorsExtension.setMessage("must be a well-formed email address");

                    errors.add(errorsExtension);
                    throw new ValidationException(errors);
                }

            } else {
                errorsExtension.setProperty("email");
                errorsExtension.setMessage("must not be null");

                errors.add(errorsExtension);
                throw new ValidationException(errors);
            }

        } else {
            errorsExtension.setProperty("password");
            errorsExtension.setMessage("size must be between 8 and 30");

            errors.add(errorsExtension);
            throw new ValidationException(errors);
        }

    }

    @Override
    @Transactional
    public void addRoleToUser(AddRoleToUserDto roleToUserDto) {

        User user = userRepository.findByUsername(roleToUserDto.getUsername())
                .orElseThrow(() -> new UserNotFoundException(roleToUserDto.getUsername()));

        UserAuthority userAuthority = authorityRepository.findByAuthority(roleToUserDto.getAuthority())
                .orElseThrow(() -> new AuthorityNotFoundException(roleToUserDto.getAuthority()));

        user.addRole(userAuthority);

        userRepository.save(user);
    }

    private User signUpToUser(SignUpDto signUpDto) {
        User user = new User();
        user.setUsername(signUpDto.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(signUpDto.getPassword()));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);

        return user;
    }


}
