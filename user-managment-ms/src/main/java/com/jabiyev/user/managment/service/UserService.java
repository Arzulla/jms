package com.jabiyev.user.managment.service;

import com.jabiyev.user.managment.domain.User;
import com.jabiyev.user.managment.service.dto.request.AddRoleToUserDto;
import com.jabiyev.user.managment.service.dto.request.SignUpDto;
import com.jabiyev.user.managment.service.dto.request.SignUpResponseDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService  extends  UserDetailsService{
    SignUpResponseDto signUp(SignUpDto signUpDto);
    void addRoleToUser(AddRoleToUserDto roleToUserDto);

}
