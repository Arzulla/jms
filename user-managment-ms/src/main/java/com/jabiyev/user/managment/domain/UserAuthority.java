package com.jabiyev.user.managment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = UserAuthority.TABLE_NAME)
public class UserAuthority implements GrantedAuthority {

    public static final String TABLE_NAME = "authorities";

    @Id
    @NotNull
    @Size(max = 100)
    @Column(length = 100,name = "name")
    private String authority;

    @Override
    public String getAuthority() {
        return authority;
    }

    @Override
    public  String toString(){
        return  this.authority;
    }
}

