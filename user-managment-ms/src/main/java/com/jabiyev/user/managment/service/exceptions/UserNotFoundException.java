package com.jabiyev.user.managment.service.exceptions;

import com.jabiyev.common.exceptions.NotFoundException;

public class UserNotFoundException extends NotFoundException {
    public static final String MESSAGE = "User with username %s is not present";
    private static final long serialVersionUID = 5843213248811L;

    public UserNotFoundException(String authority) {
        super(String.format(MESSAGE, authority));
    }
}
