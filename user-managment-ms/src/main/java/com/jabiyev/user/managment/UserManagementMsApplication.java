package com.jabiyev.user.managment;

import com.jabiyev.common.security.UserAuthorities;
import com.jabiyev.user.managment.domain.User;
import com.jabiyev.user.managment.domain.UserAuthority;
import com.jabiyev.user.managment.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;


@RequiredArgsConstructor
@SpringBootApplication
public class UserManagementMsApplication implements CommandLineRunner {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(UserManagementMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Optional<User> byUsername = userRepository.findByUsername("publisher@gmail.com");


        if(byUsername.isEmpty()){
            User user = new User();
            user.setUsername("publisher@gmail.com");
            user.setPassword(passwordEncoder.encode("publisher"));

            UserAuthority userAuthority = new UserAuthority();
            userAuthority.setAuthority(UserAuthorities.PUBLISHER.getRole());
            user.addRole(userAuthority)
            ;

            user.setAccountNonExpired(true);
            user.setEnabled(true);
            user.setAccountNonLocked(true);
            user.setCredentialsNonExpired(true);

            userRepository.save(user);
        }



    }
}
