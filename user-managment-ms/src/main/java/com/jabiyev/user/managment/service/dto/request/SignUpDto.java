package com.jabiyev.user.managment.service.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SignUpDto {

    private Long id;

    private String username;

    @NotNull
    private String password;

    @NotNull
    private String passwordConf;
}