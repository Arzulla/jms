package com.jabiyev.user.managment.service.dto.request;

import lombok.Data;

@Data
public class SignUpResponseDto {
    private Long id;

    private String username;
}
