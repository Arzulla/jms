package com.jabiyev.user.managment.service.dto.response;


public final class AccessTokenDto {
    private final String token;

    public AccessTokenDto(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }
}
