package com.jabiyev.user.managment.repository;

import com.jabiyev.user.managment.domain.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<UserAuthority, Long> {

    Optional<UserAuthority> findByAuthority(String authority);
}
