package com.jabiyev.user.managment.web.rest;


import com.jabiyev.common.security.service.jwt.JwtService;
import com.jabiyev.user.managment.domain.User;
import com.jabiyev.user.managment.service.dto.request.AddRoleToUserDto;
import com.jabiyev.user.managment.service.dto.request.LoginDto;
import com.jabiyev.user.managment.service.dto.request.SignUpDto;
import com.jabiyev.user.managment.service.dto.request.SignUpResponseDto;
import com.jabiyev.user.managment.service.dto.response.AccessTokenDto;
import com.jabiyev.user.managment.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Duration;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

    private static final Duration ONE_DAY = Duration.ofDays(1);

    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserService userService;

    @PostMapping("/authenticate")
    public ResponseEntity<AccessTokenDto> authorize(@Valid @RequestBody LoginDto loginDto) {
        log.trace("Authentication user {}", loginDto.getUsername());

        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getUsername(),
                loginDto.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        log.info("Created authtentication {}", authentication);


        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtService.issueToken(authentication, ONE_DAY);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);

        return new ResponseEntity<>(new AccessTokenDto(jwt), httpHeaders, HttpStatus.OK);

    }

    @PostMapping("/sign-up")
    public ResponseEntity<SignUpResponseDto> signUp(@RequestBody @Valid SignUpDto signUpDto) {
        return ResponseEntity.ok(userService.signUp(signUpDto));
    }

    @PostMapping("/add-role")
    public ResponseEntity addRole(@RequestBody @Valid AddRoleToUserDto roleToUserDto) {
        userService.addRoleToUser(roleToUserDto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


}
