package com.jabiyev.common.exceptions;


import java.util.ArrayList;
import java.util.List;

public class ValidationException extends RuntimeException {

    private static final String ARGUMENT_VALIDATION_FAILED = "Argument validation failed";

    private List<ErrorsExtension> errorsExtensionList=new ArrayList<>();

    public ValidationException() {
        super(ARGUMENT_VALIDATION_FAILED);
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(List<ErrorsExtension> errors) {
        super(ARGUMENT_VALIDATION_FAILED);
        errorsExtensionList=errors;
        System.out.println("errors"+errors);
    }

    public  List<ErrorsExtension> getErrorsExtensionList(){
        return errorsExtensionList;
    }


}
