package com.jabiyev.common.exceptions;


public class PasswordDoNotMatchException extends RuntimeException {

    private static final String MESSAGE = "Password do not match";

    public PasswordDoNotMatchException() {
        super(MESSAGE);
    }
}
