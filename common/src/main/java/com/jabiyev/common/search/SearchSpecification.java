package com.jabiyev.common.search;


import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.Predicate;

public class SearchSpecification<T> implements Specification<T> {

    private static final long serialVersionUID = 3522416053866116034L;

    private final List<SearchCriteria> criteriaList;
    private final MatchType matchType;

    public SearchSpecification(List<SearchCriteria> criteriaList) {
        this.criteriaList = new ArrayList<>(criteriaList);
        this.matchType = MatchType.AND;
    }

    public SearchSpecification(List<SearchCriteria> criteriaList, MatchType matchType) {
        this.criteriaList = new ArrayList<>(criteriaList);
        this.matchType = matchType;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Predicate[] predicates = criteriaList.stream()
                .map(criteria -> criteria.getOperation().buildPredicate(root, criteria, builder))
                .toArray(Predicate[]::new);
        if (matchType == MatchType.OR)
            return builder.or(predicates);
        else
            return builder.and(predicates);
    }
}
